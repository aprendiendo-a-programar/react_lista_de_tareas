import React, { useState, useEffect } from 'react';
import './App.css';
import Header from './Components/Header/Header';
import FormTask from './Components/FormTask/FormTask';
import ListTasks from './Components/ListTasks/ListTasks';

function App() {
  // Obtenemos tareas guardadas de localstorage
  const tareasGuardadas = 
    localStorage.getItem('tareas') ?
    JSON.parse(localStorage.getItem('tareas')) : [];

  // Establecemos el estado de las tareas
  const [tareas, cambiarTareas] = useState(tareasGuardadas);
  useEffect(() => {
    localStorage.setItem('tareas', JSON.stringify(tareas));
  }, [tareas]);
  
  // Acceder a localstorage y comprobamos si mostrar completadas es null
  let configMostrarCompletadas = '';
  if(localStorage.getItem('mostrarCompletadas') === null){
    configMostrarCompletadas = true;
  } else {
    configMostrarCompletadas = localStorage.getItem('mostrarCompletadas') === 'true';
  }

  // Estado de mostraCompletadas
  const [mostrarCompletadas, cambiarMostrarCompletadas] = useState(configMostrarCompletadas);

  useEffect(() => {
    localStorage.setItem('mostrarCompletadas', mostrarCompletadas.toString());
  }, [mostrarCompletadas]);

  return (
    <div className="contenedor">
      <Header
        mostrarCompletadas={mostrarCompletadas}
        cambiarMostrarCompletadas={cambiarMostrarCompletadas}
      />
      <FormTask tareas={tareas} cambiarTareas={cambiarTareas}/>
      <ListTasks
        tareas={tareas}
        cambiarTareas={cambiarTareas}
        mostrarCompletadas={mostrarCompletadas}
      />
    </div>
  );
}

export default App;
